import dom from '../../src/DOM.js'
import { App } from '../../src/App.js'
import { lt } from '../../src/Localizer.js'
import { Component } from '../../src/Component.js'
import { DataModel } from '../../src/DataModel.js'
import { DataCollection } from '../../src/DataCollection.js'

import * as components from '../../src/components/index.js'

import { MediaGridComponent } from '../../src/components/organisms/MediaGridComponent.js'

const APIDocRoot = '../../docs'
const SourceRoot = '../../docs'

class AtomsComponent extends MediaGridComponent {
	constructor(dataObject=null, options={}) {
		super(new DataCollection([
			{
				clazz: components.ButtonComponent,
				options: { text: lt('Go go') },
				doc: '/ButtonComponent.html',
				src: '/components_atoms_ButtonComponent.js.html'
			},
			{
				clazz: components.HeadingComponent,
				options: { text: 'Heading' },
				doc: '/HeadingComponent.html',
				src: '/components_atoms_HeadingComponent.js.html'
			},
			{
				clazz: components.ImageComponent,
				options: { image: '/examples/media/test-image.jpg' },
				doc: '/ImageComponent.html',
				src: '/components_atoms_ImageComponent.js.html'
			},
			{
				clazz: components.LabelComponent,
				options: { text: lt('This is a label') },
				doc: '/LabelComponent.html',
				src: '/components_atoms_LabelComponent.js.html'
			},
			{
				clazz: components.LinkComponent,
				options: {
					text: lt('This is a link'),
					url: '#nowhere'
				},
				doc: '/LinkComponent.html',
				src: '/components_atoms_LinkComponent.js.html'
			},
			{
				clazz: components.ProgressComponent,
				data: new DataModel({
					value: 0.25
				}),
				options: {
					dataField: 'value'
				},
				doc: '/ProgressComponent.html',
				src: '/components_atoms_ProgressComponent.js.html'
			},
			{
				clazz: components.SelectionComponent,
				options: {
					items: [[lt('One'), 1], [lt('Two'), 2], [lt('Three'), 3]]
				},
				doc: '/SelectionComponent.html',
				src: '/components_atoms_SelectionComponent.js.html'
			},
			{
				clazz: components.SliderComponent,
				options: { },
				doc: '/SliderComponent.html',
				src: '/components_atoms_SliderComponent.js.html'
			},
			{
				clazz: components.SwitchComponent,
				options: { },
				doc: '/SwitchComponent.html',
				src: '/components_atoms_SwitchComponent.js.html'
			},
			{
				clazz: components.TextComponent,
				options: { text: lt('This is a TextComponent') },
				doc: '/TextComponent.html',
				src: '/components_atoms_TextComponent.js.html'
			},
			{
				clazz: components.TextInputComponent,
				options: {
					placeholder: lt('Enter text here')
				},
				doc: '/TextInputComponent.html',
				src: '/components_atoms_TextInputComponent.js.html'
			},
			{
				clazz: components.ToggleComponent,
				options: { },
				doc: '/ToggleComponent.html',
				src: '/components_atoms_ToggleComponent.js.html'
			},
			{
				clazz: components.VideoComponent,
				options: {
					mimeType: 'video/mp4',
					video: '/examples/media/test16x9video.mov'
				},
				doc: '/VideoComponent.html',
				src: '/components_atoms_VideoComponent.js.html'
			}
		]), Object.assign({
			itemComponent: ComponentCardComponent
		}), options)
		this.addClass('atoms-component', 'view-component')
	}
}

class MoleculesComponent extends MediaGridComponent {
	constructor(dataObject=null, options={}) {
		super(new DataCollection([
			{
				clazz: components.AudioPlayerComponent,
				options: {
					audio: '/examples/media/test-sound.mp3'
				},
				doc: '/AudioPlayerComponent.html',
				src: '/components_molecules_AudioPlayerComponent.js.html'
			},
			{
				clazz: components.FormComponent, // TODO add field Components
				doc: '/FormComponent.html',
				src: '/components_molecules_FormComponent.js.html'
			},
			{
				clazz: components.ImageCardComponent,
				options: {
					title: lt('Image title'),
					image: '/examples/media/test-image.jpg'
				},
				doc: '/ImageCardComponent.html',
				src: '/components_molecules_ImageCardComponent.js.html'
			},
			{
				clazz: components.MenuComponent,
				doc: '/MenuComponent.html',
				src: '/components_molecules_MenuComponent.js.html'
			},
			{
				clazz: components.PaginationComponent,
				doc: '/PaginationComponent.html',
				src: '/components_molecules_PaginationComponent.js.html'
			},
			{
				clazz: components.ToolTipComponent,
				options: {
					component: new components.TextComponent(undefined, { text: 'This is the info component' })
				},
				doc: '/ToolTipComponent.html',
				src: '/components_molecules_ToolTipComponent.js.html'
			},
			{
				options: {
					mimeType: 'video/mp4',
					video: '/examples/media/test16x9video.mov'
				},
				clazz: components.VideoPlayerComponent,
				doc: '/VideoPlayerComponent.html',
				src: '/components_molecules_VideoPlayerComponent.js.html'
			},
			{
				clazz: components.WaitComponent,
				doc: '/WaitComponent.html',
				src: '/components_molecules_WaitComponent.js.html'
			}
		]), Object.assign({
			itemComponent: ComponentCardComponent
		}), options)
		this.addClass('molecules-component', 'view-component')
	}
}

class OrganismsComponent extends MediaGridComponent {
	constructor(dataObject=null, options={}) {
		super(new DataCollection([
			{
				data: new DataCollection([
					{ name: 'First item' },
					{ name: 'Second item' },
					{ name: 'Third item' }
				]),
				clazz: components.CollectionComponent,
				doc: '/CollectionComponent.html',
				src: '/components_organisms_CollectionComponent.js.html'
			},
			{
				clazz: components.MastheadComponent,
				options: {
					brand: 'Brand',
					brandAnchor: './',
					menuItems: [
						{ name: 'One', anchor: '#one' },
						{ name: 'Two', anchor: '#two' },
						{ name: 'Three', anchor: '#three' }
					]
				},
				doc: '/MastheadComponent.html',
				src: '/components_organisms_MastheadComponent.js.html'
			}
		]), Object.assign({
			itemComponent: ComponentCardComponent
		}), options)
		this.addClass('organisms-component', 'view-component')
	}
}

class SplashApp extends App {
	constructor(options={}) {
		super(options)


		this._headingComponent = new components.HeadingComponent(undefined, {
			text: lt('Components')
		}).appendTo(this)

		new components.HeadingComponent(undefined, {
			dom: dom.h2(),
			text: lt('Atoms')
		}).appendTo(this).addClass('section-heading')
		this._atomsComponent = new AtomsComponent().appendTo(this)

		new components.HeadingComponent(undefined, {
			dom: dom.h2(),
			text: lt('Molecules')
		}).appendTo(this).addClass('section-heading')
		this._moleculesComponent = new MoleculesComponent().appendTo(this)

		new components.HeadingComponent(undefined, {
			dom: dom.h2(),
			text: lt('Organisms')
		}).appendTo(this).addClass('section-heading')
		this._organismsComponent = new OrganismsComponent().appendTo(this)
	}
}

class ComponentCardComponent extends components.CardComponent {
	constructor(dataObject=null, options={}) {
		super(dataObject, Object.assign({
			dom: dom.li()
		}, options))
		this.addClass('component-card-component')

		const clazz = this.dataObject.get('clazz')
		if (!clazz) {
			console.error('Ooops', dataObject, options)
			return
		}
		const optionz = this.dataObject.get('options', null)
		const dataz = this.dataObject.get('data', null)
		const doc = this.dataObject.get('doc', null)
		const src = this.dataObject.get('src', null)

		this._targetComponent = new clazz(dataz, optionz).appendTo(this.mainComponent)
		this.titleComponent.text = this._targetComponent.dom.getAttribute('data-name')
		if (doc) {
			new components.LinkComponent(undefined, {
				text: 'API doc',
				url: APIDocRoot + doc
			}).appendTo(this.captionComponent)
		}
		if (src) {
			new components.LinkComponent(undefined, {
				text: 'Source code',
				url: SourceRoot + src
			}).appendTo(this.captionComponent)
		}
	}
}

function init() {
	const app = new SplashApp()
	window.document.body.innerHTML = ''
	window.document.body.appendChild(app.dom)
}
init()
