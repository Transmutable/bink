import js from "@eslint/js";

export default [
    {
        files: ["src/**/*.js"],
        ignores: ["docs/**/*.js"],
        languageOptions: {
            globals: {
                Blob: "readonly",
                fetch: "readonly",
                window: "readonly",
                console: "readonly",
                Response: "readonly",
                document: "readonly",
                navigator: "readonly",
                setTimeout: "readonly",
                setInterval: "readonly",
                clearTimeout: "readonly",
                clearInterval: "readonly",
            },
            parserOptions: {
                "ecmaVersion": 2017,
                "sourceType": "module"
            }
        },
        rules: {
            "no-unused-vars": "off",
            "no-undef": "warn",
        }
    }
];

